package com.example.myapplication

import android.app.Application
import com.example.myapplication.data.AlarmDataStore
import com.example.myapplication.data.AlarmDataStoreImpl
import com.example.myapplication.data.AlarmHistoryDataStore
import com.example.myapplication.data.AlarmHistoryDataStoreImpl
import com.example.myapplication.notification.AlarmNotificationHelper

class MyApplication : Application() {
    private var _alarmDataStore: AlarmDataStore? = null
    val alarmDataStore get() = _alarmDataStore!!

    private var _historyDataStore: AlarmHistoryDataStore? = null
    val historyDataStore get() = _historyDataStore!!


    private var _alarmNotificationHelper: AlarmNotificationHelper? = null
    val alarmNotificationHelper get() = _alarmNotificationHelper!!

    override fun onCreate() {
        super.onCreate()
        _alarmDataStore = AlarmDataStoreImpl(this)
        _historyDataStore = AlarmHistoryDataStoreImpl(this)
        _alarmNotificationHelper = AlarmNotificationHelper(this, alarmDataStore, historyDataStore)
        alarmNotificationHelper.createNotificationChannel()
    }
}