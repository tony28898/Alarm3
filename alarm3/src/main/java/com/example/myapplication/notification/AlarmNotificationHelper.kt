package com.example.myapplication.notification

import android.Manifest
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.example.myapplication.AlarmReceiver
import com.example.myapplication.MainActivity
import com.example.myapplication.R
import com.example.myapplication.data.*
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

class AlarmNotificationHelper(
    private val context: Context,
    private val alarmStore: AlarmDataStore,
    private val historyStore: AlarmHistoryDataStore
) {
    companion object {
        private const val CHANNEL_ID = "channel_id_alarm"
    }

    private val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")
    var mediaPlayer = MediaPlayer.create(context, R.raw.alarm_sound)

    fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = context.getString(R.string.channel_name)
            val descriptionText = context.getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun scheduleAlarm(alarm: Alarm) {
        val alarmManager = context.getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager

        // Create an intent to trigger the alarm receiver
        val intent = Intent(context, AlarmReceiver::class.java)
            .putExtra("alarmAction", AlarmAction(alarm, AlarmAction.Action.BUZZ))

        // Create a PendingIntent to be triggered when the alarm goes off
        val pendingIntent = PendingIntent.getBroadcast(
            context,
            alarm.id + 100, // unique requestCode for distinguish and cancel PendingIntents
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )
        // cancel any existing scheduled alarm with the same alarm ID
        alarmManager.cancel(pendingIntent)

        val triggerTime = if (LocalTime.now() >= alarm.localTime) {
            LocalDateTime.of(LocalDate.now().plusDays(1), alarm.localTime)
        } else {
            LocalDateTime.of(LocalDate.now(), alarm.localTime)
        }.toEpochMilli()
        alarmManager.setAlarmClock(
            AlarmManager.AlarmClockInfo(
                triggerTime,
                pendingIntent
            ), pendingIntent
        )
    }

    fun removeAlarm(alarm: Alarm) {
        val alarmManager = context.getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager

        // Create an intent to trigger the alarm receiver
        val intent = Intent(context, AlarmReceiver::class.java)
            .putExtra("alarmAction", AlarmAction(alarm, AlarmAction.Action.BUZZ))

        // Create a PendingIntent to be triggered when the alarm goes off
        val pendingIntent = PendingIntent.getBroadcast(
            context,
            alarm.id + 100, // unique requestCode for distinguish and cancel PendingIntents
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )
        // cancel any existing scheduled alarm with the same alarm ID
        alarmManager.cancel(pendingIntent)

    }

    fun sendAlarmBuzzNotification(alarm: Alarm) {
        if (ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) return
        val intent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent =
            PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_IMMUTABLE)
        val title = alarm.localTime.format(timeFormatter)
        val message =
            context.getString(R.string.notification_message).replace("{label}", alarm.label)
        val sound =
            RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)    // change to a longer music

        val snoozePendingIntent = PendingIntent.getBroadcast(
            context,
            0,
            Intent(context, AlarmReceiver::class.java)
                .putExtra("alarmAction", AlarmAction(alarm, AlarmAction.Action.SNOOZE)),
            PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_IMMUTABLE
        )
        val dismissPendingIntent = PendingIntent.getBroadcast(
            context,
            1,
            Intent(context, AlarmReceiver::class.java)
                .putExtra("alarmAction", AlarmAction(alarm, AlarmAction.Action.DISMISS)),
            PendingIntent.FLAG_ONE_SHOT or PendingIntent.FLAG_IMMUTABLE
        )

        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle(title)
            .setContentText(message)
            .setContentIntent(pendingIntent)
            .setVibrate(longArrayOf(1000, 1000))    // try fine-tune the pattern
            .setSound(sound)
            .setOngoing(true)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .addAction(
                NotificationCompat.Action(
                    R.drawable.baseline_snooze_24,
                    context.getString(R.string.action_snooze_alarm),
                    snoozePendingIntent
                )
            )
            .addAction(
                NotificationCompat.Action(
                    R.drawable.baseline_cancel_24,
                    context.getString(R.string.action_dismiss_alarm),
                    dismissPendingIntent
                )
            )
        with(NotificationManagerCompat.from(context)) {
            val notificationId = alarm.id
            notify(notificationId, builder.build())
        }
        mediaPlayer.start()
    }

    fun snoozeAlarm(alarm: Alarm) {
        with(NotificationManagerCompat.from(context)) {
            val notificationId = alarm.id
            cancel(notificationId)
        }
        removeAlarm(alarm)
        val currentLocalTime = LocalTime.now().plus(1, ChronoUnit.MINUTES)
        scheduleAlarm(alarm.newAlarmWithTime(currentLocalTime))
        alarmStore.updateSnoozeCount(alarm.id, 1)
        //TODO : Updatesnoozecount passing -1 as alarmid
        Log.d("Log", "Snoozing alarm:" + alarm.id)
    }

    fun dismissAlarm(alarm: Alarm) {
        with(NotificationManagerCompat.from(context)) {
            val notificationId = alarm.id
            cancel(notificationId)
        }
        removeAlarm(alarm)
        val originalAlarm = alarmStore.getAlarm(alarm.id)
        if (originalAlarm != null) {
            // assume dismiss time - ring time < 24h
            val ringTime =
                originalAlarm.localTime.atDate(LocalDate.now()).toEpochMilli()
            val dismissTime = LocalDateTime.now().toEpochMilli()
            historyStore.add(AlarmHistoryMeta(originalAlarm, ringTime, dismissTime))
        }
        alarmStore.getAlarm(alarm.id)?.let {
            scheduleAlarm(it)
        }
        mediaPlayer.stop()
        mediaPlayer.prepare()
    }

    private fun checkNotificationPermission(
        activity: Activity,
        requestPermissionLauncher: ActivityResultLauncher<String>
    ) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) return
        when {
            ActivityCompat.checkSelfPermission(
                activity,
                Manifest.permission.POST_NOTIFICATIONS
            ) == PackageManager.PERMISSION_GRANTED -> {

            }
            shouldShowRequestPermissionRationale(
                activity,
                Manifest.permission.POST_NOTIFICATIONS
            ) -> {

            }
            else -> {
                requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }

    private fun LocalDateTime.toEpochMilli() =
        atZone(ZoneId.systemDefault()).toOffsetDateTime().toInstant().toEpochMilli()
}