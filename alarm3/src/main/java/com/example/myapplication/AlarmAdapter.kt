package com.example.myapplication

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.data.Alarm
import com.example.myapplication.databinding.ItemAlarmBinding
import java.time.format.DateTimeFormatter

class AlarmAdapter : ListAdapter<Alarm, AlarmAdapter.ViewHolder>(diffUtil) {

    var onItemClickListener: OnItemClickListener? = null
    var onEnableClickListener: OnEnableClickListener? = null

    companion object {
        val diffUtil = object : DiffUtil.ItemCallback<Alarm>() {
            override fun areItemsTheSame(oldItem: Alarm, newItem: Alarm): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Alarm, newItem: Alarm): Boolean {
                return oldItem == newItem
            }
        }
        private val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")
    }

    fun interface OnItemClickListener {
        fun onItemClick(alarm: Alarm)
    }

    fun interface OnEnableClickListener {
        fun onEnableClick(alarm: Alarm)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemAlarmBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val alarm = getItem(position)
        holder.timeText.text = alarm.localTime.format(timeFormatter)
        holder.labelText.text = alarm.label
        holder.snoozeCountText.text = holder.itemView.context.getString(R.string.snooze_count)
            .replace("{count}", alarm.snoozeCount.toString())
        holder.enableSwitch.isChecked = alarm.enabled

        holder.itemView.setOnClickListener {
            onItemClickListener?.onItemClick(alarm)
        }
        holder.enableSwitch.setOnClickListener {
            onEnableClickListener?.onEnableClick(alarm)
        }
    }

    inner class ViewHolder(binding: ItemAlarmBinding) : RecyclerView.ViewHolder(binding.root) {
        val timeText = binding.timeText
        val labelText = binding.labelText
        val snoozeCountText = binding.snoozeCountText
        val enableSwitch = binding.enableSwitch
    }
}