package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.data.Alarm
import com.example.myapplication.databinding.ActivityMainBinding
import com.example.myapplication.screen.EditAlarmDialogFragment
import com.example.myapplication.screen.MyViewModelFactory

class MainActivity : AppCompatActivity(),
    AlarmAdapter.OnItemClickListener,
    AlarmAdapter.OnEnableClickListener {
    private lateinit var binding: ActivityMainBinding
    private val viewModel by viewModels<MainViewModel> { MyViewModelFactory(myApplication) }

    private val requestPermissionLauncher =
        registerForActivityResult(RequestPermission()) { isGranted ->
            if (isGranted) {

            } else {

            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val adapter = AlarmAdapter()
        adapter.onItemClickListener = this
        adapter.onEnableClickListener = this
        binding.alarmList.adapter = adapter
        binding.alarmList.layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, true).apply {
                stackFromEnd = true
            }

        binding.addAlarmButton.setOnClickListener {
            EditAlarmDialogFragment().apply {
                onSaveClick = EditAlarmDialogFragment.OnSaveClickListener { alarm ->
                    viewModel.addAlarm(alarm)
                }
            }.show(supportFragmentManager, EditAlarmDialogFragment::class.java.name)
        }

        viewModel.alarms.observe(this) {
            adapter.submitList(it)
        }
    }

    override fun onItemClick(alarm: Alarm) {
        EditAlarmDialogFragment.newInstance(alarm).apply {
            onSaveClick = EditAlarmDialogFragment.OnSaveClickListener { alarm ->
                viewModel.updateAlarm(alarm)
            }
            onDeleteClick = {
                viewModel.removeAlarm(alarm)
            }
        }.show(supportFragmentManager, EditAlarmDialogFragment::class.java.name)
    }

    override fun onEnableClick(alarm: Alarm) {
        viewModel.toggleAlarmEnable(alarm)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.history_button -> {
                val intent = Intent(this,AlarmHistoryActivity::class.java)
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}