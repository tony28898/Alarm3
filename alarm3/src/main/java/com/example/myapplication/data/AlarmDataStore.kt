package com.example.myapplication.data

import kotlinx.coroutines.flow.Flow

interface AlarmDataStore {
    fun getAlarms(): Flow<List<Alarm>>

    fun getAlarm(id: Int): Alarm?
    fun upsert(alarm: Alarm):Int

    fun updateSnoozeCount(alarmId: Int, increment: Int)
    fun remove(alarm: Alarm)
    fun removeAll()
}