package com.example.myapplication.data

import kotlinx.serialization.Serializable

@Serializable
data class AlarmHistory(
    val id: Int,
    val alarm: Alarm,
    val alarmRingTime: Long,
    val alarmDismissTime: Long
) {
    companion object {
        fun withMeta(id: Int, meta: AlarmHistoryMeta) = AlarmHistory(
            id,
            meta.alarm,
            meta.alarmRingTime,
            meta.alarmDismissTime
        )
    }
}

data class AlarmHistoryMeta(
    val alarm: Alarm,
    val alarmRingTime: Long,
    val alarmDismissTime: Long
)