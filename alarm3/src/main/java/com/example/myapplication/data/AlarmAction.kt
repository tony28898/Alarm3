package com.example.myapplication.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AlarmAction(val alarm: Alarm, val action: Action) : Parcelable {
    enum class Action {
        BUZZ, SNOOZE, DISMISS
    }
}