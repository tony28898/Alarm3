package com.example.myapplication.data

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class AlarmHistoryDataStoreImpl(context: Context) : AlarmHistoryDataStore {
    private val counterKey = "historyCounter"
    private val storeKey = "alarmHistory"
    private val sp = context.getSharedPreferences("alarmCollection", Context.MODE_PRIVATE)
    private var memory = sp.getAlarmHistory()

    private var counter = sp.getInt(counterKey, 0)
    private val historyFlow = MutableStateFlow(memory)

    override fun getAlarmHistoryList(): Flow<List<AlarmHistory>> {
        return historyFlow
    }

    override fun getAlarmHistoryList(id: Int): AlarmHistory? {
        return memory.find { it.id == id }
    }

    override fun add(history: AlarmHistoryMeta): Int {
        val newHistory = AlarmHistory.withMeta(counter, history)
        memory = memory + newHistory
        counter++
        notifyAndWriteToStorage()
        return newHistory.id
    }

    override fun update(history: AlarmHistory): Int {
        val oldIdx = memory.indexOfFirst { it.id == history.id }
        if (oldIdx >= 0) {
            memory = memory.toMutableList().apply {
                removeAt(oldIdx)
                add(oldIdx, history)
            }
        }
        notifyAndWriteToStorage()
        return history.id
    }

    override fun remove(history: AlarmHistory) {
        val mutCopy = memory.toMutableList()
        if (mutCopy.remove(history)) {
            memory = mutCopy
            notifyAndWriteToStorage()
        }
    }

    override fun removeAll() {
        memory = listOf()
        notifyAndWriteToStorage()
    }

    private fun notifyAndWriteToStorage() {
        historyFlow.value = memory
        sp.writeHistory(memory)
    }

    private fun SharedPreferences.writeHistory(history: List<AlarmHistory>) {
        val json = Json.encodeToString(history)
        edit {
            putInt(counterKey, counter)
            putString(storeKey, json)
        }
    }


    private fun SharedPreferences.getAlarmHistory(): List<AlarmHistory> {
        val jsonString = getString(storeKey, "[]")!!
        return Json.decodeFromString(jsonString)
    }
}