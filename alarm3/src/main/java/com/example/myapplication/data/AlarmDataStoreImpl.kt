package com.example.myapplication.data

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.edit
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class AlarmDataStoreImpl(context: Context) : AlarmDataStore {
    private val counterKey = "counter"
    private val storeKey = "alarms"
    private val sp = context.getSharedPreferences("alarmCollection", Context.MODE_PRIVATE)
    private var memory = sp.getAlarms()

    private var counter = sp.getInt(counterKey, 0)
    private val alarmsFlow = MutableStateFlow(memory)

    override fun getAlarms(): Flow<List<Alarm>> {
        return alarmsFlow
    }

    override fun getAlarm(id: Int): Alarm? {
        return memory.find { it.id == id }
    }

    override fun upsert(alarm: Alarm): Int {
        val oldIdx = memory.indexOfFirst { it.id == alarm.id }
        if (oldIdx < 0) {
            memory = memory + alarm.copy(id = counter)
            Log.d("Log", "Adding alarm Memory ID:" + counter)
            counter++
        } else {
            memory = memory.toMutableList().apply {
                removeAt(oldIdx)
                Log.d("Log", "Removing alarm Memory ID:" + oldIdx)
                add(oldIdx, alarm)
                Log.d("Log", "Adding alarm Memory ID" + oldIdx)
            }
        }
        notifyAndWriteToStorage()
        return if (oldIdx < 0) {
            counter - 1
        } else oldIdx
    }

    override fun updateSnoozeCount(alarmId: Int, increment: Int) {
        val oldAlarm = memory.find { it.id == alarmId } ?: return
        val newAlarm = oldAlarm.copy(snoozeCount = oldAlarm.snoozeCount + increment)
        upsert(newAlarm)
    }

    override fun remove(alarm: Alarm) {
        val mutCopy = memory.toMutableList()
        if (mutCopy.remove(alarm)) {
            Log.d("Log", "Removing alarm:" + alarm.id)
            memory = mutCopy
            notifyAndWriteToStorage()
        }
    }

    override fun removeAll() {
        memory = listOf()
        notifyAndWriteToStorage()
    }

    private fun notifyAndWriteToStorage() {
        alarmsFlow.value = memory
        sp.writeAlarms(memory)
    }

    private fun SharedPreferences.writeAlarms(alarms: List<Alarm>) {
        val json = Json.encodeToString(alarms)
        edit {
            putInt(counterKey, counter)
            putString(storeKey, json)
        }
    }

    private fun SharedPreferences.getAlarms(): List<Alarm> {
        val jsonString = getString(storeKey, "[]")!!
        return Json.decodeFromString(jsonString)
    }

}