package com.example.myapplication.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import java.time.LocalTime

@Parcelize
@Serializable
data class Alarm(
    var id: Int = NO_ID,
    val hour: Int,
    val minute: Int,
    val label: String,
    val enabled: Boolean = true,
    val snoozeCount: Int = 0
) : Parcelable {
    @Transient
    val localTime = LocalTime.of(hour, minute)

    fun newAlarmWithTime(time: LocalTime) = copy(
        hour = time.hour,
        minute = time.minute
    )
}
const val NO_ID = -1