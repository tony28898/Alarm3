package com.example.myapplication.data

import kotlinx.coroutines.flow.Flow

interface AlarmHistoryDataStore {
    fun getAlarmHistoryList(): Flow<List<AlarmHistory>>
    fun getAlarmHistoryList(id: Int): AlarmHistory?
    fun add(history: AlarmHistoryMeta): Int
    fun update(history: AlarmHistory): Int
    fun remove(history: AlarmHistory)
    fun removeAll()
}