package com.example.myapplication

import android.content.Context
import com.example.myapplication.data.AlarmDataStore
import com.example.myapplication.data.AlarmDataStoreImpl

val Context.myApplication get() = this.applicationContext as MyApplication