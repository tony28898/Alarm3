package com.example.myapplication

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.example.myapplication.data.AlarmHistoryDataStore

class HistoryViewModel(
    private val dataStore: AlarmHistoryDataStore
) : ViewModel() {
    val history = dataStore.getAlarmHistoryList().asLiveData()
}