package com.example.myapplication.screen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.HistoryViewModel
import com.example.myapplication.MainViewModel
import com.example.myapplication.MyApplication

@Suppress("UNCHECKED_CAST")
class MyViewModelFactory(private val application: MyApplication) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when (modelClass) {
            MainViewModel::class.java -> {
                MainViewModel(application.alarmDataStore, application.alarmNotificationHelper) as T
            }
            HistoryViewModel::class.java -> {
                HistoryViewModel(application.historyDataStore) as T
            }
            else -> throw IllegalArgumentException("Unknown modelClass $modelClass")
        }
    }
}