package com.example.myapplication.screen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import com.example.myapplication.data.Alarm
import com.example.myapplication.data.NO_ID
import com.example.myapplication.databinding.DialogAlarmBinding

class EditAlarmDialogFragment : DialogFragment() {

    fun interface OnSaveClickListener {
        fun onSaveClick(alarm: Alarm)
    }

    companion object {
        private const val paramKey = "alarm"
        fun newInstance(alarm: Alarm) = EditAlarmDialogFragment().apply {
            arguments = Bundle().apply {
                putParcelable(paramKey, alarm)
            }
        }
    }

    private var _binding: DialogAlarmBinding? = null
    private val binding get() = _binding!!

    var onDeleteClick: (() -> Unit)? = null
    var onSaveClick: OnSaveClickListener? = null

    private var alarm: Alarm? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.getParcelable<Alarm>(paramKey)?.let {
            alarm = it
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DialogAlarmBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            alarmTimePicker.setIs24HourView(true)
            dialogAlarmSaveButton.setOnClickListener {
                onSaveClick?.onSaveClick(
                    Alarm(
                        id = alarm?.id ?: NO_ID,
                        hour = alarmTimePicker.hour,
                        minute = alarmTimePicker.minute,
                        label = dialogAlarmName.text.toString()
                    )
                )
                dismiss()
            }
            dialogAlarmDeleteButton.setOnClickListener {
                onDeleteClick?.invoke()
                dismiss()
            }
            dialogAlarmDeleteButton.isVisible = alarm != null
            alarm?.let {
                dialogAlarmName.setText(it.label)
                alarmTimePicker.hour = it.hour
                alarmTimePicker.minute = it.minute
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}