package com.example.myapplication

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.example.myapplication.data.Alarm
import com.example.myapplication.data.AlarmDataStore
import com.example.myapplication.notification.AlarmNotificationHelper

class MainViewModel(
    private val dataStore: AlarmDataStore,
    private val alarmHelper: AlarmNotificationHelper
) : ViewModel() {
    val alarms = dataStore.getAlarms().asLiveData()

    fun addAlarm(alarm: Alarm) {

        var newAlarmId = dataStore.upsert(alarm)
        alarm.id = newAlarmId
        Log.d("Log","Scheduling:"+ alarm.id)
        alarmHelper.scheduleAlarm(alarm)
    }

    fun updateAlarm(alarm: Alarm) {
        Log.d("Log","Updating:"+alarm.id )
        dataStore.upsert(alarm)
        alarmHelper.scheduleAlarm(alarm)
    }

    fun removeAlarm(alarm: Alarm) {
        Log.d("Log","Removing:" + alarm.id )
        dataStore.remove(alarm)
        alarmHelper.removeAlarm(alarm)
    }

    fun toggleAlarmEnable(alarm: Alarm) {
        val shouldEnable = !alarm.enabled
        val newAlarm = alarm.copy(enabled = shouldEnable)
        dataStore.upsert(newAlarm)
        if (shouldEnable) {
            alarmHelper.scheduleAlarm(newAlarm)
        } else {
            alarmHelper.removeAlarm(alarm)
        }
    }
}