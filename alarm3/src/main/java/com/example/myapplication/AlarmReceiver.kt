package com.example.myapplication

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.myapplication.data.AlarmAction

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val alarmAction = intent.getParcelableExtra<AlarmAction>("alarmAction")!!
        when (alarmAction.action) {
            AlarmAction.Action.BUZZ -> {
                context.myApplication.alarmNotificationHelper.sendAlarmBuzzNotification(alarmAction.alarm)
            }
            AlarmAction.Action.SNOOZE -> {
                context.myApplication.alarmNotificationHelper.snoozeAlarm(alarmAction.alarm)
            }
            AlarmAction.Action.DISMISS -> {
                context.myApplication.alarmNotificationHelper.dismissAlarm(alarmAction.alarm)
            }
        }
    }
}