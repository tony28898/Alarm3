package com.example.myapplication

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.data.Alarm
import com.example.myapplication.data.AlarmHistory
import com.example.myapplication.databinding.ItemAlarmBinding
import com.example.myapplication.databinding.ItemAlarmHistoryBinding
import java.time.format.DateTimeFormatter

class AlarmHistoryAdapter: ListAdapter<AlarmHistory, AlarmHistoryAdapter.ViewHolder>(diffUtil){

    companion object {
        val diffUtil = object : DiffUtil.ItemCallback<AlarmHistory>() {
            override fun areItemsTheSame(oldItem: AlarmHistory, newItem: AlarmHistory): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: AlarmHistory, newItem: AlarmHistory): Boolean {
                return oldItem == newItem
            }

        }
        private val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlarmHistoryAdapter.ViewHolder {
        return ViewHolder(
            ItemAlarmHistoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val alarmHistory = getItem(position)
        holder.timeText.text = alarmHistory.alarm.localTime.format(AlarmHistoryAdapter.timeFormatter)
        holder.labelText.text = alarmHistory.alarm.label
        holder.timeDifferenceText.text = ((alarmHistory.alarmDismissTime - alarmHistory.alarmRingTime)/1000).toString() + " Second"
    }


    inner class ViewHolder(binding: ItemAlarmHistoryBinding) : RecyclerView.ViewHolder(binding.root) {
        val timeText = binding.timeText
        val timeDifferenceText = binding.timeDifferenceText
        val labelText = binding.labelText
    }
}