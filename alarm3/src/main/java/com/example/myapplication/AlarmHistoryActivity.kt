package com.example.myapplication

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.ActivityAlarmHistoryBinding
import com.example.myapplication.screen.MyViewModelFactory

class AlarmHistoryActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAlarmHistoryBinding
    private val viewModel by viewModels<HistoryViewModel> { MyViewModelFactory(myApplication) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAlarmHistoryBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.historyToolbar)

        val adapter = AlarmHistoryAdapter()
        binding.historyList.adapter = adapter
        binding.historyList.layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, true).apply {
                stackFromEnd = true
            }

        viewModel.history.observe(this) {
            adapter.submitList(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_history, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.back_button -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}