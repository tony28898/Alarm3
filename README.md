# Alarm Snooze time counter

## Scenario

This project served as an exploration of Android mobile app development, allowing me to expand my skills in this domain.

The application encompasses the following functionalities:

- 1. Alarm Management: Users can easily set, modify, and delete alarms, with the added benefit of the alarm actually going off as intended!
- 2. Notification System: The app sends notifications with options to snooze or remove the alarm.
- 3. Database: The alarm details are stored in built-in database.
- 4. Alarm Snooze Counter: Users can track the number of times an alarm has been snoozed before it is eventually removed.
- 5. History Page: The app includes a dedicated page displaying previous alarm records, providing users with a convenient overview.

## Screenshots
<img src="https://i.ibb.co/hHsrk8K/Screenshot-2023-10-17-at-20-22-28.png" width="300">
<img src="https://i.ibb.co/TBC2qqh/Screenshot-2023-10-17-at-20-22-07.png" width="300" >
<img src="https://i.ibb.co/25qBHVb/Screenshot-2023-10-17-at-20-23-23.png" width="300">

